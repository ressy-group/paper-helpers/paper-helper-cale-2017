# N90-VRC38 Antibody and inferred ancestor sequences from Cale 2017

<https://doi.org/10.1016/j.immuni.2017.04.011>

 * GenBank entries for N90-VRC38 lineage member sequences:
   * heavy: KY905214-KY905227
   * light: KY905228-KY905238
 * PDB entries:
   * N90-VRC38.01 Fab: 5EWI
   * V1V2-scaffold complex: 5VGJ
