#!/usr/bin/env python

"""
Parse text from figure 6B or Table S6 into FASTA.
"""

import re
import sys

def parse_line(line):
    """Get seq IDs and seqs from plaintext of figure/table"""
    match = re.match(" *([^ ]+) (.*)", line)
    seqid, seq = match.groups()
    seq = seq.replace(" ", "")
    return seqid, seq

def make_explicit(pairs):
    """Replace - in each sequence with first symbol encountered in that column"""
    default = {}
    out = []
    for seqid, seq in pairs:
        # update defaults
        for idx, sym in enumerate(seq):
            if idx not in default:
                default[idx] = sym
        # apply default anywhere there's a - (not a gap in this context)
        seq = "".join([default[idx] if sym == "-" else sym for idx, sym in enumerate(seq)])
        out.append((seqid, seq))
    return out

def ljust(pairs):
    """Left-justify sequences into proper alignment"""
    amt = max(len(seq) for _, seq in pairs)
    pairs_out = []
    for seqid, seq in pairs:
        seq += "-"*(amt - len(seq))
        pairs_out.append((seqid, seq))
    return pairs_out

def parse_txt(path, path_out):
    with open(path) as f_in, open(path_out, "wt") as f_out:
        pairs = []
        for line in f_in:
            pairs.append(parse_line(line))
        pairs = make_explicit(pairs)
        pairs = ljust(pairs)
        for seqid, seq in pairs:
            f_out.write(f">{seqid}\n{seq}\n")

if __name__ == "__main__":
    parse_txt(sys.argv[1], sys.argv[2])
