#!/usr/bin/env python

"""
Gather heavy and light chain sequences from the paper's figure/table into a CSV file.
"""

import re
import sys
from csv import DictWriter
from Bio import SeqIO

def make_parsed_sheet(path_heavy, path_light, path_out):
    seqdict = lambda path: {rec.id: str(rec.seq) for rec in SeqIO.parse(path, "fasta")}
    heavy = seqdict(path_heavy)
    light = seqdict(path_light)
    keyfilt = lambda txt: not re.match(r"IG[HKL]V[0-9]-[0-9]+\*[0-9]+", txt)
    keys = {key for key in heavy if keyfilt(key)}
    keys = keys | {key for key in light if keyfilt(key)}

    intermediates = {key for key in keys if re.match("I[A0-9]+", key)}
    mature = {key for key in keys if re.match("VRC38", key)}
    if len(intermediates) > len(mature):
        keys = {key for key in keys if not re.match("VRC38", key)}

    keys = sorted(list(keys), key=lambda txt: int(re.sub("[^0-9]", "", txt) or 0))
    with open(path_out, "w") as f_out:
        writer = DictWriter(f_out, ["Entry", "HeavyAA", "LightAA"], lineterminator="\n")
        writer.writeheader()
        for key in keys:
            writer.writerow({"Entry": key, "HeavyAA": heavy.get(key), "LightAA": light.get(key)})

if __name__ == "__main__":
    make_parsed_sheet(sys.argv[1], sys.argv[2], sys.argv[3])
