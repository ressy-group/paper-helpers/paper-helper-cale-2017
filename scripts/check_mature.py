#!/usr/bin/env python

"""
Check my copy of paper's AA sequences against translated GenBank entries.

VRC38.07 evidently has one light chain AA differing between GenBank and table
S6.
"""

import sys
from csv import DictReader
from Bio import SeqIO
from Bio.Seq import Seq

def check_mature(parsed_csv, genbank_csv):
    genbank_aa = {}
    with open(genbank_csv) as f_in:
        for row in DictReader(f_in):
            genbank_aa[(row["Antibody"], row["Chain"])] = str(Seq(row["Seq"]).translate())
    err = 0
    with open(parsed_csv) as f_in:
        for row in DictReader(f_in):
            key = (f"N90-{row['Entry']}", "heavy")
            if row["HeavyAA"] != genbank_aa[key]:
                sys.stderr.write(f"Mismatch for {key}\n")
                err = 1
            key = (f"N90-{row['Entry']}", "light")
            if row["LightAA"] != genbank_aa[key]:
                sys.stderr.write(f"Mismatch for {key}\n")
                err = 1
    return err

if __name__ == "__main__":
    sys.exit(check_mature(sys.argv[1], sys.argv[2]))
