with open("from-paper/genbank_accessions.txt") as f_in:
    GB_ACCESSIONS = [line.strip() for line in f_in]

TARGET_GBF_FASTA = expand("from-genbank/{acc}.fasta", acc=GB_ACCESSIONS)
TARGET_PARSED = expand("parsed/{thing}_{suffix}.fasta", thing=["fig6B", "tableS6"], suffix=["intermediates", "mature"])

rule all:
    input: expand("output/{thing}.csv", thing=["seqs", "parsed_seqs", "parsed_intermediates"])

rule all_gbf_fasta:
    input: TARGET_GBF_FASTA

rule all_gbf:
    input: TARGET_GBF_FASTA

rule make_seqs_aa_parsed_sheet:
    output: "output/parsed_seqs.csv"
    input:
        heavy="parsed/fig6B_mature.fasta",
        light="parsed/tableS6_mature.fasta"
    shell: "python scripts/make_parsed_sheet.py {input.heavy} {input.light} {output}"

rule make_intermediates_sheet:
    output: "output/parsed_intermediates.csv"
    input:
        heavy="parsed/fig6B_intermediates.fasta",
        light="parsed/tableS6_intermediates.fasta"
    shell: "python scripts/make_parsed_sheet.py {input.heavy} {input.light} {output}"

rule all_parsed:
    input: TARGET_PARSED

rule parse_seqs:
    output: "parsed/{thing}.fasta"
    input: "from-paper/{thing}.txt"
    shell: "python scripts/parse_txt.py {input} {output}"

rule make_seqs_sheet:
    output: "output/seqs.csv"
    input: fastas=TARGET_GBF_FASTA
    shell: "python scripts/make_seqs_sheet.py {input.fastas} > {output}"

rule download_gbf_fa:
    """Download one FASTA file per GenBank accession."""
    output: "from-genbank/{acc}.fasta"
    shell: "python scripts/download_ncbi.py nucleotide {wildcards.acc} {output}"
